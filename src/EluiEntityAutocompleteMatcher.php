<?php

namespace Drupal\elui;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media_entity\MediaInterface;

/**
 * Class EluiEntityAutocompleteMatcher.
 *
 * @package Drupal\elui
 */
class EluiEntityAutocompleteMatcher extends EntityAutocompleteMatcher {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    $matches = [];

    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, 10);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $label = $this->getLabel($entity_id, $target_type, $label);
          // Strip things like starting/trailing white spaces, line breaks and
          // tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($label)))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);
          $matches[] = ['value' => $key, 'label' => $label];
        }
      }
    }

    return $matches;
  }

  /**
   * Add unpublished to label if content is unpublished.
   */
  protected function getLabel($entity_id, $entity_type_id, $label) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    $label = $label . ' (' . $entity_id . ')';
    if (($entity instanceof EntityPublishedInterface || $entity instanceof MediaInterface) && !$entity->isPublished()) {
      $label = $this->t('Unpublished') . ' - ' . $label;
    }
    return $label;
  }

}
