<?php

namespace Drupal\Tests\elui\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;

/**
 * Tests entity unpublished indicator for autocomplete.
 *
 * @group elui
 */
class EluiAutocompleteTest extends BrowserTestBase {

  use EntityReferenceTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'elui'];

  /**
   * The autocomplete URL to call.
   *
   * @var string
   */
  protected $autocompleteUrl;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create the "Article" node type.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Create a field of an entity reference field storage on the "Article"
    // bundle.
    $this->createEntityReferenceField(
      'node',
      'article',
      'field_ref',
      NULL,
      'node',
      'default',
      [
        'target_bundles' => [
          'article' => 'article',
        ],
      ],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );
    $this->rebuildAll();

    EntityFormDisplay::load('node.article.default')
      ->setComponent('field_ref', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->save();

    // Create the nodes for testing.
    Node::create(['title' => 'Test node', 'type' => 'article'])
      ->setPublished()
      ->save();
    Node::create(['title' => 'Test node', 'type' => 'article'])
      ->setUnpublished()
      ->save();

    // Create a user and then login (grant 'bypass node access' permission
    // because of https://www.drupal.org/project/drupal/issues/2845144).
    $adminUser = $this->drupalCreateUser(['bypass node access']);
    $this->drupalLogin($adminUser);

    // Retrieve the autocomplete url.
    $this->drupalGet('node/add/article');
    $result = $this->xpath('//input[@name="field_ref[0][target_id]"]');
    $this->autocompleteUrl = $this->getAbsoluteUrl($result[0]->getAttribute('data-autocomplete-path'));
  }

  /**
   * Tests autocomplete results.
   */
  public function testAutocompleteResults() {
    $query_options = ['query' => ['q' => 'test']];
    $response = Json::decode($this->drupalGet($this->autocompleteUrl, $query_options));

    $this->assertStringStartsWith('Test node', $response[0]['value']);
    $this->assertStringStartsWith('Unpublished - Test node', $response[1]['value']);
  }

}
